# NIM Fixed Point Number library

fpn is a library for [fixed point number](https://en.wikipedia.org/wiki/Fixed-point_arithmetic) in NIM.

It offers a similar interface to the standard math functions for use on fixed point numbers.

You can create fpn type with `genQM_N(fpn_type, n_fract_bits, type`

- fpn_type is the name of your type (ex. initQ16_16)
- n_fract_bits the number of fractional bits stored
- type between fixedPoint8, fixedPoint16, fixedPoint32, fixedPoint64

## Example

Add two numbers:

```
var a = initQ16_16()
var b = initQ16_16()

a.fromFloat(40.4)
b.fromFloat(1.6)
echo add(a, b).toInt()
# 42
```

Create a new fpn type:

```
genQM_N(initQ4_12, 12, fixedPoint16)
var q = initQ4_12()
q.toInt() # 0 == type int16
# q.fromFloat(4.2)
echo q.toInt() # 4 == type int16
echo q # (data: 17203) == type fixedPoint16[12]
echo q.toFloat() # 4.199951171875 == type float
```

See tests for more example.

## API Documentation

Documentation is available at https://lbartoletti.gitlab.io/fpn/fpn.html

- https://lbartoletti.gitlab.io/fpn/fpn/fpn.html (main module)
- https://lbartoletti.gitlab.io/fpn/fpn/utils.html (some utils methods)

## How to contribute?

There is a TODO list if you wish to contribute.
Improve existing documentation and tests.
Please document new methods and provide a test for them.

PR are welcomed!

## TODO

- [ ] A full README :)
- [ ] Add build Options (no float / max 8-16-32 bits ...)
- [ ] Trigonometry functions (cordic in progress in another library ; maybe LUT and Taylor series in another library)
- [ ] More tests (only Q16_16 is tested)
- [ ] Bindings (C and Python planned)
