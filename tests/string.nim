import unittest

import fpn

test "string to fp":
  var a = initQ16_16()
  var b = initQ16_16()
  var c = initQ16_16()
  var aReference = a
  var bReference = b
  var cReference = c

  a.fromString("1.3")
  b.fromString("0.25")
  c.fromString("2")

  aReference.fromFloat(1.3)
  bReference.fromFloat(0.25)
  cReference.fromInt(2)

  let one = b / (ipow(c, 14)) # rawData = 1

  check a == aReference - one # fromString truncates, fromFloat rounds up
  check b == bReference
  check c == cReference

test "fp to string":
  var a = initQ16_16()
  var b = initQ16_16()
  var c = initQ16_16()

  a.fromFloat(-5.125) # negative number
  b = high(b) # very high number
  c.fromInt(123) # integer

  check $a == "-5.125"
  check $b == "32767.999984741"
  check $c == "123.0"
