import unittest

import fpn

test "ilog":
  var a = initQ16_16()
  var b = initQ16_16()
  var c = initQ16_16()
  var d = initQ16_16()
  a.fromInt(10)
  b.fromInt(100)
  c.fromInt(2)
  d.fromInt(3)
  let e = a.e

  check ilog(a, e) == c # 2
  check ilog(a, c) == d # 3
  check ilog(b, a) == c # 2

  check ilog2(a) == d # 3
  check ilog2(c) == a.one
  check ilog2(d) == a.one

  check ilog10(a) == a.one
  check ilog10(b) == c # 2
  check ilog10(c) == a.zero
