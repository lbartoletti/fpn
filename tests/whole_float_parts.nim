import unittest

import fpn

test "whole and float parts":
  var a = initQ16_16()
  var resWhole = initQ16_16()
  var resFloat = initQ16_16()
  var w = initQ16_16()
  var f = initQ16_16()

  check a.wholePart() == a
  check a.floatPart() == a

  a.fromFloat(4.0)
  resWhole.fromInt(4)
  resFloat.fromInt(0)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == resFloat

  a.fromFloat(4.5)
  resWhole.fromInt(4)
  resFloat.fromFloat(0.5)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == resFloat

  a.fromFloat(4.6)
  resFloat.fromFloat(0.6)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == resFloat

  a.fromFloat(4.3)
  resFloat.fromFloat(0.3)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == resFloat

  a.fromFloat(-4.5)
  resWhole.fromInt(-4)
  resFloat.fromFloat(0.5)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == -resFloat

  a.fromFloat(-4.6)
  resFloat.fromFloat(0.6)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == -resFloat

  a.fromFloat(-4.3)
  resFloat.fromFloat(0.3)
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == -resFloat

  a.fromInt(high(int16))
  resFloat.fromFloat(0.0)
  resWhole.fromInt(high(int16))
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == resFloat

  a.fromInt(low(int16))
  resFloat.fromFloat(0.0)
  resWhole.fromInt(low(int16))
  check a.wholePart() == resWhole
  check a.floatPart() == resFloat
  (w, f) = a.modf()
  check w == resWhole
  check f == resFloat
