import unittest, math
import fpn

const Epsilon = 1 # Margin of error for the tests

test "pi":
  var Pi4_4Float: fixedPoint8[4]
  var Pi8_8Float: fixedPoint16[8]
  var Pi16_16Float: fixedPoint32[16]
  var Pi32_32Float: fixedPoint64[32]

  let Pi4_4FP = Pi4_4Float.pi
  let Pi8_8FP = Pi8_8Float.pi
  let Pi16_16FP = Pi16_16Float.pi
  let Pi32_32FP = Pi32_32Float.pi

  Pi4_4Float.fromFloat(PI)
  Pi8_8Float.fromFloat(PI)
  Pi16_16Float.fromFloat(PI)
  Pi32_32Float.fromFloat(PI)

  echo Pi4_4FP.toFloat
  echo Pi8_8FP.toFloat
  echo Pi16_16FP.toFloat
  # echo Pi32_32FP.toFloat

  check abs((Pi4_4Float - Pi4_4FP).rawData) <= Epsilon
  check abs((Pi8_8Float - Pi8_8FP).rawData) <= Epsilon
  check abs((Pi16_16Float - Pi16_16FP).rawData) <= Epsilon
  # The following likely won't be the same, due to rounding inconsistencies
  check abs((Pi32_32Float - Pi32_32FP).rawData) <= Epsilon

test "e":
  var E4_4Float: fixedPoint8[4]
  var E8_8Float: fixedPoint16[8]
  var E16_16Float: fixedPoint32[16]
  var E32_32Float: fixedPoint64[32]

  let E4_4FP = E4_4Float.e
  let E8_8FP = E8_8Float.e
  let E16_16FP = E16_16Float.e
  let E32_32FP = E32_32Float.e

  E4_4Float.fromFloat(E)
  E8_8Float.fromFloat(E)
  E16_16Float.fromFloat(E)
  E32_32Float.fromFloat(E)

  echo E4_4FP.toFloat
  echo E8_8FP.toFloat
  echo E16_16FP.toFloat
  # echo E32_32FP.toFloat

  check abs((E4_4Float - E4_4FP).rawData) <= Epsilon
  check abs((E8_8Float - E8_8FP).rawData) <= Epsilon
  check abs((E16_16Float - E16_16FP).rawData) <= Epsilon
  # The following likely won't be the same, due to rounding inconsistencies
  check abs((E32_32Float - E32_32FP).rawData) <= Epsilon
