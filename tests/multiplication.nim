import unittest

import math
import strutils

import fpn

test "[over|under]flow_mul":
  var a = initQ16_16()
  var b = initQ16_16()

  check not isOverflowMul(a,b)
  check not isUnderflowMul(a,b)
  check isSafeMul(a,b)

  a.fromInt(high(int16))
  b.fromInt(2)
  check isOverflowMul(a,b)
  check not isUnderflowMul(a,b)
  check isSafeMul(a,b) == false
  check toInt(a.ovMul(b)) == -2
  check a.ovMul(b) == a.ovAdd(a)

  a.fromInt(low(int16))
  b.fromInt(-1)
  check not isUnderflowMul(a,b)
  check isOverflowMul(a,b)
  check isSafeMul(a,b) == false
  check toInt(a.ovMul(b)) == low(int16)

  b.fromInt(-2)
  check not isUnderflowMul(a,b)
  check isOverflowMul(a,b)
  check isSafeMul(a,b) == false
  check toInt(a.ovMul(b)) == 0

test "multiplication":
  var a = initQ16_16()
  var b = initQ16_16()
  check mul(a, b).rawData() == 0
  check rawData(a * b) == 0
  a *= b
  check a.rawData() == 0

  a.fromInt(4)
  b.fromInt(6)
  check mul(a, b).toInt() == 24
  check mul(a, b).toFloat() == 24.0

test "saturated multiplication":
  var a = initQ16_16()
  var b = initQ16_16()
  a.fromInt(high(int16)-10)
  b.fromInt(2)
  check satMul(a, b).toInt() == high(int16)

  a.fromInt(low(int16))
  b.fromInt(2)
  check satMul(a, b).toInt() == low(int16)

test "square number":
  var a = initQ16_16()
  var res = initQ16_16()

  a.fromInt(3)
  res.fromInt(3*3)
  check square(a) == res

test "cube number":
  var a = initQ16_16()
  var res = initQ16_16()

  a.fromInt(3)
  res.fromInt(3*3*3)
  check cube(a) == res

test "multiplication long":
  var a = initQ16_16()
  var b = initQ16_16()
  check ovMulst(a, b).rawData() == 0
  check rawData(a * b) == 0
  a *= b
  check a.rawData() == 0

  a.fromInt(4)
  b.fromInt(6)
  check ovMulSt(a, b).toInt() == 24
  check ovMulSt(a, b).toFloat() == 24.0

  a.fromFloat(E)
  b.fromFloat(6.0)
  check format("%0.2f", ovMulSt(a, b).toFloat()) == format("%0.2f", E*6)

test "int multiplication":
  var a = initQ16_16()
  var b = 4
  var c = initQ16_16()
  a.fromFloat(0.25)
  c.fromInt(1)
  check a * b == c
  check b * a == c
  a *= b
  check a == c
