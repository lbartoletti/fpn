import unittest

import fpn

test "floor":
  var a = initQ16_16()
  var res = initQ16_16()

  check a.floor() == a

  a.fromFloat(4.0)
  res.fromInt(4)
  check a.floor() == res

  a.fromFloat(4.5)
  res.fromInt(4)
  check a.floor() == res

  a.fromFloat(4.6)
  check a.floor() == res

  a.fromFloat(4.3)
  check a.floor() == res

  a.fromFloat(-4.5)
  res.fromInt(-5)
  check a.floor() == res

  a.fromFloat(-4.6)
  check a.floor() == res

  a.fromFloat(-4.3)
  check a.floor() == res

  a.fromInt(high(int16))
  res.fromInt(high(int16))
  check a.floor() == res

  a.fromInt(low(int16))
  res.fromInt(low(int16))
  check a.floor() == res
