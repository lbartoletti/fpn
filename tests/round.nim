import unittest

import fpn

test "roundHalfUp":
  var a = initQ16_16()
  var res = initQ16_16()

  check a.roundHalfUp() == a

  a.fromFloat(4.0)
  res.fromInt(4)
  check a.roundHalfUp() == res

  a.fromFloat(4.5)
  res.fromInt(5)
  check a.roundHalfUp() == res

  a.fromFloat(4.6)
  check a.roundHalfUp() == res

  a.fromFloat(4.3)
  res.fromInt(4)
  check a.roundHalfUp() == res

  a.fromFloat(-4.5)
  res.fromInt(-4)
  check a.roundHalfUp() == res

  a.fromFloat(-4.6)
  res.fromInt(-5)
  check a.roundHalfUp() == res

  a.fromFloat(-4.3)
  res.fromInt(-4)
  check a.roundHalfUp() == res

  a.fromInt(high(int16))
  res.fromInt(high(int16))
  check a.roundHalfUp() == res

  a.fromInt(low(int16))
  res.fromInt(low(int16))
  check a.roundHalfUp() == res

test "roundHalfDown":
  var a = initQ16_16()
  var res = initQ16_16()

  check a.roundHalfDown() == a

  a.fromFloat(4.0)
  res.fromInt(4)
  check a.roundHalfDown() == res

  a.fromFloat(4.5)
  res.fromInt(5)
  check a.roundHalfDown() == res

  a.fromFloat(4.6)
  check a.roundHalfDown() == res

  a.fromFloat(4.3)
  res.fromInt(4)
  check a.roundHalfDown() == res

  a.fromFloat(-4.5)
  res.fromInt(-5)
  check a.roundHalfDown() == res

  a.fromFloat(-4.6)
  check a.roundHalfDown() == res

  a.fromFloat(-4.3)
  res.fromInt(-4)
  check a.roundHalfDown() == res

  a.fromInt(high(int16))
  res.fromInt(high(int16))
  check a.roundHalfDown() == res

  a.fromInt(low(int16))
  res.fromInt(low(int16))
  check a.roundHalfDown() == res
