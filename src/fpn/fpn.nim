import utils
from strutils import split, parseInt

template genQType(name: untyped, tbits: typed) =
  type
    name*[fracBits: static[int]] {.inject.} = object
      data: tbits

template genQM_N*(name: untyped, n: static[int], qtype: typed) =
  func `name`*(data = 0, frac = 0): qtype[n] =
    result.data = typeof(result.data)(data) shl (n - frac)

genQType(fixedPoint8, int8)
genQType(fixedPoint16, int16)
genQType(fixedPoint32, int32)
genQType(fixedPoint64, int64)

type FixedPoint = fixedPoint8 | fixedPoint16 | fixedPoint32 | fixedPoint64;

func fromSameType*(a: FixedPoint, data = 0, frac = 0): typeof(a) =
  ## Creates a new fixedPoint from the same type as a
  result.data = typeof(result.data)(data) shl (a.rawFracBits() - frac)

# Ceil/floor
proc floor*(a: FixedPoint): FixedPoint =
  ## Returns the floor number from `a`
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   a.fromFloat(4.6)
  ##   echo floor(a).toFloat() ## 4.0
  ##   a.fromFloat(-4.6)
  ##   echo floor(a).toFloat() ## -4.0
  ##
  result.data = typeof(a.data)(a.data and not a.fracMask)

proc ceil*(a: FixedPoint): FixedPoint =
  ## Returns the ceil number from `a`
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   a.fromFloat(4.6)
  ##   echo floor(a).toFloat() ## 5.0
  ##   a.fromFloat(-4.6)
  ##   echo floor(a).toFloat() ## -4.0
  ##
  if ( a.data and a.fracMask ) == 0:
    result.data = a.data
  else:
    result.data = typeof(a.data)((a.data + (1 shl a.fracBits)) and not a.fracMask)

# Converters
proc fromInt*(fp: var FixedPoint, v: SomeInteger) =
  ## Converts an integer to the fixed point
  ##
  ## v must be in the range of fp.data
  fp.data = typeof(fp.data)(v) shl fp.fracBits

proc toInt*(fp: FixedPoint): SomeInteger =
  ## Converts the fixed point data to an integer
  var rounded = fp
  if fp.isNeg:
    rounded = ceil(fp)
  rounded.data shr fp.fracBits

proc floatToFixedNumber(fp: FixedPoint, v: SomeFloat) : SomeInteger =
  ## Convenient method to converts a float to the fixed point with fracBits
  var round: typeof(v) = 0.5
  if v < 0.0:
    round = -0.5
  let one = 1 shl fp.fracBits
  typeof(fp.data)(toFloat(one) * v + round)

proc fromString*(fp: var FixedPoint, str: string) =
  ## Converts a string to a fixed point
  ## Example:
  ## var a = initQ16_16()
  ## var b = initQ16_16()
  ## a.fromString(a, "1.25")
  ## b.fromString(a, "1.75")
  ## echo a + b # 3.0
  var decimal: BiggestInt = 1
  var fracPart {.noInit.}: typeof(fp.data)
  let strSplit = str.split('.')
  fp.fromInt(parseInt(strSplit[0]))

  if len(strSplit) != 1:
    for i in countup(1, min(16, len(strSplit[1]))):
      decimal *= 10
    fracPart = typeof(fp.data)(((parseInt(strSplit[1]) shl fp.fracBits) div decimal))
    if fp.data < 0:
      fracPart = -fracPart
    fp.data += fracPart

proc `$`*(fp: FixedPoint): string =
  ## Dollar proc. Returns the input converted to a string.
  ## Example:
  ## var a = initQ16_16()
  ## a.fromFloat(-2.5)
  ## echo $a # "-2.5"
  let whole = fp.toInt
  if fp.isNeg and whole == 0: result.add('-')
  result.add($whole & ".")
  var fracPart: BiggestInt = fp.floatPart.data
  if fracPart != 0:
    var hi = 1_000_000_000 # replace with 10^18 for more precision
    fracPart = (fracPart * hi) shr fp.fracBits
    hi = hi div 10
    while fracPart < hi:
      result.add('0')
      hi = hi div 10
    # Remove trailing zeros
    while fracPart mod 10 == 0:
      fracPart = fracPart div 10
  result.add($fracPart)

proc fromFloat*(fp: var FixedPoint, v: SomeFloat) =
  ## Converts a float to the fixed point
  fp.data = floatToFixedNumber(fp, v)

proc toFloat*(fp: FixedPoint): float =
  ## Converts the fixed point data to a float
  let one = (1 shl fp.fracBits)
  toFloat(fp.data) / toFloat(one)

# Getters
proc rawData*(fp: FixedPoint): SomeInteger =
  ## Gets the raw data in fp
  fp.data

proc rawFracBits*(fp: FixedPoint): SomeInteger =
  ## Gets the fractional bits in fp
  fp.fracBits

# comparisons
proc `<`*(a, b: FixedPoint): bool =
  ## pre-condition a and b are of the same type
  ##
  ## Returns true if `x` is less than `b`
  a.data < b.data

proc `>`*(a, b: FixedPoint): bool =
  ## pre-condition a and b are of the same type
  ##
  ## Returns true if `x` is greater than `b`
  a.data > b.data

proc `<=`*(a, b: FixedPoint): bool =
  ## pre-condition a and b are of the same type
  ##
  ## Returns true if `x` is less or equal than `b`
  a.data <= b.data

proc `>=`*(a, b: FixedPoint): bool =
  ## pre-condition a and b are of the same type
  ##
  ## Returns true if `x` is greater or equal than `b`
  a.data >= b.data

proc isNeg*(a: FixedPoint): bool =
  ## Returns true if a is negative
  a.data < 0

proc fracMask*(a: FixedPoint): SomeInteger =
      typeof(a.data) ( (1 shl a.fracBits) - 1)

proc isOne*(a: FixedPoint): bool =
  ## Returns true if a is one
  a.data == typeof(a.data)(1 shl a.fracBits)

proc one*(a: FixedPoint): FixedPoint =
  ## Returns a fixed point from the same type as a with value One
  result.data = typeof(a.data)(1 shl a.fracBits)

proc isZero*(a: FixedPoint): bool =
  ## Returns true if a is one
  a.data == 0

proc zero*(a: FixedPoint): FixedPoint =
  ## Returns a fixed point from the same type as a with value Zero
  result.data = 0

proc e*(a: FixedPoint): FixedPoint =
  ## Returns a fixed point from the same type as a with value E
  # e with 32 fractional bits
  const e_int = 0b10_10110_11111_10000_10101_00010_11000_10
  let shift = 32 - a.rawFracBits()
  if shift < 0:
    result.data = typeof(result.data)(e_int shl -shift)
  else:
    result.data = typeof(result.data)(e_int shr shift)

proc pi*(a: FixedPoint): FixedPoint =
  ## Returns a fixed point from the same type as a with value PI
  # Pi with 32 fractional bits
  const pi_int = 0b11_00100_10000_11111_10110_10101_00010_00
  let shift = 32 - a.rawFracBits()
  if shift < 0:
    result.data = typeof(result.data)(pi_int shl -shift)
  else:
    result.data = typeof(result.data)(pi_int shr shift)

proc low*(a: FixedPoint): FixedPoint =
  ## Returns a fixed point from the same type as a with lowest value
  result.data = low(typeof(a.data))

proc high*(a: FixedPoint): FixedPoint =
  ## Returns a fixed point from the same type as a with highest value
  result.data = high(typeof(a.data))

# abs
proc isSafeAbs*(a: FixedPoint): bool =
  ## See:
  ##  * `isSafeAbs proc <#isSafeAbs,SomeInteger>`_
  return isSafeAbs(a.data)

proc ovAbs*(a: FixedPoint): FixedPoint =
  ## Returns the absolute value of `a`.
  ## This method can overflow the value of `a`.
  ##
  ## See also:
  ##  * `isSafeAbs proc <#isSafeAbs,FixedPoint>`_
  ##  * `satAbs proc <#isSafeAbs,FixedPoint>`_
  ##  * `abs proc <#isSafeAbs,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##    var a = initQ16_16()
  ##    a.fromInt(-5)
  ##    echo ovAbs(a).toInt() ## 5
  ##    a.fromInt(low(int16))
  ##    echo ovAbs(a).toInt() ## high(int16)
  ##
  if isSafeAbs(a):
    result.data = abs(a.data)
  else:
    result.data = high(typeof(a.data))

proc satAbs*(a: FixedPoint): FixedPoint =
  ## Returns the absolute value of `a`.
  ## This method saturate the value of `a` if isSafeAbs is false.
  ##
  ## See also:
  ##  * `isSafeAbs proc <#isSafeAbs,FixedPoint>`_
  ##  * `ovAbs proc <#isSafeAbs,FixedPoint>`_
  ##  * `abs proc <#isSafeAbs,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   a.fromInt(-5)
  ##   echo satAbs(a).toInt() ## 5
  ##   a.fromInt(low(int16))
  ##   echo satAbs(a).toInt() ## low(int16)
  ##
  if isSafeAbs(a):
    result.data = abs(a.data)
  else:
    result.data = low(typeof(a.data))

proc abs*(a: FixedPoint): FixedPoint =
  ## Returns the absolute value of `a`.
  ## This method doesn't perform overflow verification (aka fastAbs).
  ##
  ## See also:
  ##  * `isSafeAbs proc <#isSafeAbs,FixedPoint>`_
  ##  * `ovAbs proc <#isSafeAbs,FixedPoint>`_
  ##  * `satAbs proc <#isSafeAbs,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   a.fromInt(-5)
  ##   echo abs(a).toInt() ## 5
  ##   a.fromInt(low(int16))
  ##   echo abs(a).toInt() ## low(int16)
  ##
  result.data = abs(a.data)

# Negate
proc isSafeNegate*(a: FixedPoint): bool =
  ## See:
  ##  * `isSafeNegate proc <#isSafeNegate,SomeInteger>`_
  return isSafeNegate(a.data)

proc `-`*(a: FixedPoint): FixedPoint =
  ## Returns the negative number of `a`
  ##
  ## See also:
  ##  * `isSafeNegate proc <#isSafeNegate,SomeInteger>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   a.fromInt(5)
  ##   echo a.SafeNegate().toInt() ## -5
  ##   a.fromInt(-5)
  ##   echo a.SafeNegate().toInt() ## 5
  ##   a.fromInt(low(int16))
  ##   echo a.SafeNegate().toInt() ## OverflowError
  ##
  result.data = -a.data

# Addition
proc isUnderflowAdd*(a, b: FixedPoint): bool =
  ## Returns true if `a` + `b` produces an underflow error
  return isUnderflowAdd(a.data, b.data)

proc isOverflowAdd*(a, b: FixedPoint): bool =
  ## Returns true if `a` + `b` produces an overflow error
  return isOverflowAdd(a.data, b.data)

proc isSafeAdd*(a, b: FixedPoint): bool =
  ## Returns true if `a` + `b` is safe (no under- or overflow error)
  ##
  ## See also:
  ##  * `isUnderflowAdd proc <#isUnderflowAdd,FixedPoint>`_
  ##  * `isOverflowAdd proc <#isOverflowAdd,FixedPoint>`_
  return isSafeAdd(a.data, b.data)

proc add*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` + `b`.
  ## This method doesn't perform overflow verification (aka fastAdd).
  ##
  ## See also:
  ##  * `isUnderflowAdd proc <#isUnderflowAdd,FixedPoint>`_
  ##  * `isOverflowAdd proc <#isOverflowAdd,FixedPoint>`_
  ##  * `isSafeAdd proc <#isSafeAdd,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(5)
  ##   b.fromInt(37)
  ##   echo add(a, b) ## 42
  ##   b.fromInt(high(int16))
  ##   echo add(a, b) ## OverflowError
  ##
  result.data = a.data + b.data

proc satAdd*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` + `b`.
  ## This method saturate the value of `a` + `b` if isSafeAdd is false.
  ##
  ## See also:
  ##  * `isUnderflowAdd proc <#isUnderflowAdd,FixedPoint>`_
  ##  * `isOverflowAdd proc <#isOverflowAdd,FixedPoint>`_
  ##  * `add proc <#add,FixedPoint>`_
  ##  * `satAdd proc <#satAdd,FixedPoint>`_
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(5)
  ##   b.fromInt(high(int16))
  ##   echo satAdd(a, b).toInt() ## high(int16)
  ##   b.fromInt(low(int16))
  ##   echo satAdd(-a, b).toInt() ## low(int16)
  ##
  if isOverflowAdd(a, b):
    result.data = high(typeof(a.data))
  elif isUnderflowAdd(a, b):
    result.data = low(typeof(a.data))
  else:
    result.data = a.data + b.data

proc ovAdd*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` + `b`.
  ## This method overflow the value of `a` + `b` if isSafeAdd is false.
  ##
  ## See also:
  ##  * `isUnderflowAdd proc <#isUnderflowAdd,FixedPoint>`_
  ##  * `isOverflowAdd proc <#isOverflowAdd,FixedPoint>`_
  ##  * `add proc <#add,FixedPoint>`_
  ##  * `satAdd proc <#satAdd,FixedPoint>`_
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(1)
  ##   b.fromInt(high(int16))
  ##   echo ovAdd(a, b).toInt() ## low(int16)
  ##   b.fromInt(low(int16))
  ##   echo add(-a, b).toInt() ## high(int16)
  ##
  result.data = ovAdd(a.data, b.data)

proc `+`*(a, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  return ovAdd(a, b)

proc `+`*(a: SomeInteger, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  var afp = fromSameType(b)
  afp.fromInt(a)
  return ovAdd(afp, b)

proc `+`*(a: FixedPoint, b: SomeInteger): FixedPoint =
  ## See
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  var bfp = fromSameType(a)
  bfp.fromInt(b)
  return ovAdd(a, bfp)

template `+=`*(a: var FixedPoint, b: FixedPoint): untyped =
  ## See
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  a = a + b

template `+=`*(a: var FixedPoint, b: SomeInteger): untyped =
  ## See
  ##  * `ovAdd proc <#ovAdd,FixedPoint>`_
  a = a + b

# Substraction
proc isUnderflowSub*(a, b: FixedPoint): bool =
  # Returns true if `a` - `b` produces an underflow error
  return isUnderflowSub(a.data, b.data)

proc isOverflowSub*(a, b: FixedPoint): bool =
  # Returns true if `a` - `b` produces an overflow error
  return isOverflowSub(a.data, b.data)

proc isSafeSub*(a, b: FixedPoint): bool =
  ## Returns true if `a` - `b` is safe (no under- or overflow error)
  ##
  ## See also:
  ##  * `isUnderflowSub proc <#isUnderflowSub,FixedPoint>`_
  ##  * `isOverflowSub proc <#isOverflowSub,FixedPoint>`_
  isSafeSub(a.data, b.data)

proc sub*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` - `b`.
  ## This method doesn't perform overflow verification (aka fastSub).
  ##
  ## See also:
  ##  * `isUnderflowSub proc <#isUnderflowSub,FixedPoint>`_
  ##  * `isOverflowSub proc <#isOverflowSub,FixedPoint>`_
  ##  * `isSafeSub proc <#isSafeSub,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(47)
  ##   b.fromInt(5)
  ##   echo sub(a, b) ## 42
  ##   a.fromInt(low(int16))
  ##   b.fromInt(5)
  ##   echo sub(a, b) ## OverflowError
  ##
  result.data = a.data - b.data

proc satSub*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` - `b`.
  ## This method saturate the value of `a` - `b` if isSafeSub is false.
  ##
  ## See also:
  ##  * `isUnderflowSub proc <#isUnderflowSub,FixedPoint>`_
  ##  * `isOverflowSub proc <#isOverflowSub,FixedPoint>`_
  ##  * `sub proc <#sub,FixedPoint>`_
  ##  * `satSub proc <#satSub,FixedPoint>`_
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(low(int16))
  ##   b.fromInt(5)
  ##   echo satSub(a, b).toInt() ## low(int16)
  ##
  if isOverflowSub(a, b):
    result.data = high(typeof(a.data))
  elif isUnderflowSub(a, b):
    result.data = low(typeof(a.data))
  else:
    result.data = a.data - b.data

proc ovSub*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` - `b`.
  ## This method overflow the value of `a` - `b` if isSafeSub is false.
  ##
  ## See also:
  ##  * `isUnderflowSub proc <#isUnderflowSub,FixedPoint>`_
  ##  * `isOverflowSub proc <#isOverflowSub,FixedPoint>`_
  ##  * `sub proc <#sub,FixedPoint>`_
  ##  * `satSub proc <#satSub,FixedPoint>`_
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(low(int16))
  ##   b.fromInt(1)
  ##   echo ovSub(a, b).toInt() ## high(int16)
  ##
  result.data = ovSub(a.data, b.data)

proc `-`*(a, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  return ovSub(a, b)

proc `-`*(a: SomeInteger, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  var afp = fromSameType(b)
  afp.fromInt(a)
  return ovSub(afp, b)

proc `-`*(a: FixedPoint, b: SomeInteger): FixedPoint =
  ## See
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  var bfp = fromSameType(a)
  bfp.fromInt(b)
  return ovSub(a, bfp)

template `-=`*(a: var FixedPoint, b: FixedPoint): untyped =
  ## See
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  a = a - b

template `-=`*(a: var FixedPoint, b: SomeInteger): untyped =
  ## See
  ##  * `ovSub proc <#ovSub,FixedPoint>`_
  a = a - b

# Multiplication
proc isOverflowMul*(a, b: FixedPoint) : bool =
  # Returns true if `a` *`b` produces an overflow error
  isOverflowMul(a.data, b.data)

proc isUnderflowMul*(a, b: FixedPoint) : bool =
  # Returns true if `a` * `b` produces an underflow error
  isUnderflowMul(a.data, b.data)

proc isSafeMul*(a, b: FixedPoint): bool =
  ## Returns true if `a` * `b` is safe (no under- or overflow error)
  isSafeMul(a.data, b.data)

proc mul*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` * `b`.
  ## This method doesn't perform overflow verification (aka fastMul).
  ##
  ## See also:
  ##  * `isUnderflowMul proc <#isUnderflowMul,FixedPoint>`_
  ##  * `isOverflowMul proc <#isOverflowMul,FixedPoint>`_
  ##  * `isSafeMul proc <#isSafeMul,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(7)
  ##   b.fromInt(6)
  ##   echo mul(a, b) ## 42
  ##   a.fromInt(high(int16))
  ##   b.fromInt(5)
  ##   echo mul(a, b) ## OverflowError
  ##
  var res: BiggestUInt = BiggestUInt(a.data) * BiggestUInt(b.data)
  result.data = typeof(a.data)(res shr ( (a.fracBits + b.fracBits) - a.fracBits ))

proc ovMul*(a, b: FixedPoint): FixedPoint =
  var res = ( karatsuba(a.data, b.data) shr a.fracBits )
  if res > high(typeof(a.data)):
    var divide = res div high(typeof(a.data))
    var carry = res mod high(typeof(a.data))
    while divide > 0:
      result.data = ovAdd(high(typeof(a.data)), result.data)
      divide -= 1
    result.data = ovAdd(result.data, typeof(a.data)carry)
  else:
    result.data = typeof(a.data)res

proc ovMulst*(a, b: FixedPoint): FixedPoint =
  var a_h = a.data shr a.fracBits
  var b_h = b.data shr b.fracBits

  var a_l = a.data and a.fracMask
  var b_l= b.data and b.fracMask

  result.data += (a_h * b_h) shl a.fracBits
  result.data += (a_h * b_l)
  result.data += (b_h * a_l)
  result.data += ( (a_l * b_l) shl a.fracBits) and a.fracMask

proc ovMulNaive*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` * `b`.
  ## This method overflow the value of `a` * `b` if isSafeMul is false.
  ##
  ## See also:
  ##  * `isUnderflowMul proc <#isUnderflowMul,FixedPoint>`_
  ##  * `isOverflowMul proc <#isOverflowMul,FixedPoint>`_
  ##  * `sub proc <#sub,FixedPoint>`_
  ##  * `satMul proc <#satMul,FixedPoint>`_
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(low(int16))
  ##   b.fromInt(-1)
  ##   echo ovMul(a, b).toInt() ## low(int16)
  ##
  var i = fromSameType(a)

  let pa = ovAbs(a)
  let pb = ovAbs(b)

  if pa < pb:
    while i < pa:
      i = satAdd(i, a.one())
      result += b
  else:
    while i < pb:
      i = satAdd(i, a.one())
      result += a

proc satMul*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` * `b`.
  ## This method saturate the value of `a` * `b` if isSafeMul is false.
  ##
  ## See also:
  ##  * `isUnderflowMul proc <#isUnderflowMul,FixedPoint>`_
  ##  * `isOverflowMul proc <#isOverflowMul,FixedPoint>`_
  ##  * `mul proc <#mul,FixedPoint>`_
  ##  * `satMul proc <#satMul,FixedPoint>`_
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(low(int16))
  ##   b.fromInt(5)
  ##   echo satMul(a, b).toInt() ## low(int16)
  ##
  if isOverflowMul(a, b):
    result.data = high(typeof(a.data))
  elif isUnderflowMul(a, b):
    result.data = low(typeof(a.data))
  else:
    result.data = a.data * b.data

proc `*`*(a, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  return ovMul(a, b)

proc `*`*(a: FixedPoint, b: SomeInteger): FixedPoint =
  ## See
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  var bfp = fromSameType(a) # Transforms b into a fpn
  bfp.fromInt(b)
  return ovMul(a, bfp)

proc `*`*(a: SomeInteger, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  var afp = fromSameType(b) # Transforms b into a fpn
  afp.fromInt(a)
  return ovMul(afp, b)

template `*=`*(a: var FixedPoint, b: FixedPoint): untyped =
  ## See
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  a = a * b

template `*=`*(a: var FixedPoint, b: SomeInteger): untyped =
  ## See
  ##  * `ovMul proc <#ovMul,FixedPoint>`_
  a = a * b

proc square*(a: FixedPoint): FixedPoint =
  # Returns the square number of `a`
  # This method uses ovMul to compute the square number
  a * a

proc cube*(a: FixedPoint): FixedPoint =
  # Returns the cube number of `a`
  # This method uses ovMul to compute the cube number
  a * a * a

# Division
proc isOverflowDiv*(a, b: FixedPoint) : bool =
  # Returns true if `a` / `b` produces an overflow error
  isOverflowDiv(a.data, b.toInt())

proc isUnderflowDiv*(a, b: FixedPoint) : bool =
  # Returns true if `a` / `b` produces an underflow error...
  # Wait... no. Returns false
  false

proc isSafeDiv*(a, b: FixedPoint) : bool =
  ## Returns true if `a` / `b` is safe (no under- or overflow error) and b != 0
  isSafeDiv(a.data, b.toInt())

proc ovDiv*(a, b: FixedPoint) : FixedPoint =
  ## Returns the result of `a` / `b`.
  ## This method overflow the value of `a` / `b` if isSafeDiv is false.
  ##
  ## See also:
  ##  * `isUnderflowDiv proc <#isUnderflowDiv,FixedPoint>`_
  ##  * `isOverflowDiv proc <#isOverflowDiv,FixedPoint>`_
  ##  * `sub proc <#sub,FixedPoint>`_
  ##  * `satDiv proc <#satDiv,FixedPoint>`_
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(low(int16))
  ##   b.fromInt(-1)
  ##   echo ovDiv(a, b).toInt() ## high(int16)
  ##

  # if b == 0 : returns high(int) or low(int) depending sign of a and b
  if b.isZero():
    if ( (a.data and a.fracMask) xor (b.data and b.fracMask) ) == 0:
      return high(a)
    else:
      return low(a)

  # low(int) / -1 => high(int)
  if isOverflowDiv(a, b):
    result.data = high(typeof(a.data))
    return result

  var res : BiggestInt
  res = BiggestInt(a.data) * a.one().rawData()
  res = res div BiggestInt(b.data)

  if res > high(typeof(a.data)):
    var divide = res div high(typeof(a.data))
    var carry = res mod high(typeof(a.data))
    while divide > 0:
      result.data = ovAdd(high(typeof(a.data)), result.data)
      divide -= 1
    result.data = ovAdd(result.data, typeof(a.data)carry)
  else:
    result.data = typeof(a.data)res

proc satDiv*(a, b: FixedPoint): FixedPoint =
  ## Returns the result of `a` / `b`.
  ## This method saturate the value of `a` / `b` if isSafeDiv is false.
  ##
  ## See also:
  ##  * `isUnderflowDiv proc <#isUnderflowDiv,FixedPoint>`_
  ##  * `isOverflowDiv proc <#isOverflowDiv,FixedPoint>`_
  ##  * `mul proc <#mul,FixedPoint>`_
  ##  * `satDiv proc <#satDiv,FixedPoint>`_
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var b = initQ16_16()
  ##   a.fromInt(low(int16))
  ##   b.fromInt(-1)
  ##   echo satDiv(a, b).toInt() ## low(int16)
  ##
  if not isSafeDiv(a, b):
    result.data = low(typeof(a.data))
  else:
    return ovDiv(a, b)

proc `/`*(a, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  return ovDiv(a, b)

proc `/`*(a: SomeInteger, b: FixedPoint): FixedPoint =
  ## See
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  var afp = fromSameType(b)
  afp.fromInt(a)
  return ovDiv(afp, b)

proc `/`*(a: FixedPoint, b: SomeInteger): FixedPoint =
  ## See
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  var bfp = fromSameType(a)
  bfp.fromInt(b)
  return ovDiv(a, bfp)

proc `div`*(a, b: FixedPoint) : FixedPoint =
  ## See
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  a / b

template `/=`*(a: var FixedPoint, b: FixedPoint): untyped =
  ## See
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  a = a / b

template `/=`*(a: var FixedPoint, b: SomeInteger): untyped =
  ## See
  ##  * `ovDiv proc <#ovDiv,FixedPoint>`_
  a = a / b

proc floatPart*(a: FixedPoint): FixedPoint =
  ## Returns the float part of `a`
  ## Returns a positive number even if `a` is negative. Use modf to get a negative number
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   a.fromFloat(4.6)
  ##   echo floatPart(a).toFloat() ## ~ 0.6
  ##   a.fromFloat(-4.3)
  ##   echo floatPart(a).toFloat() ## ~ 0.3
  ##
  if a.data >= 0 or a.data == low(typeof(a.data)):
    result.data = a.data and a.fracMask
  else:
    result.data = (-a.data) and a.fracMask

proc wholePart*(a: FixedPoint): FixedPoint =
  ## Returns the whole part of `a`
  ## Returns floor(a) if `a` is potive else ceil(a)
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  if a.data > 0:
    return floor(a)
  else:
    return ceil(a)

proc roundHalfUp*(a: FixedPoint): FixedPoint =
  ## Returns the smallest integer that is not less than `a`
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  var half = fromSameType(a)
  half.fromFloat(0.5) # TODO: Remove this float
  result = satAdd(a, half)
  result = floor(result)

proc roundHalfDown*(a: FixedPoint): FixedPoint =
  ## Returns the largest integer that does not exceed `a`
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  var half = fromSameType(a)
  half.fromFloat(0.5) # TODO: Remove this float
  if a.data > 0:
    return wholePart(satAdd(a,half))
  else:
    return wholePart(satSub(a,half))

proc modf*(a: FixedPoint): (FixedPoint, FixedPoint) =
  ## Returns the whole and the float part of `a`
  ## Returns a negative number if `a` is negative.
  ##
  ## See also:
  ##  * `floor proc <#floor,FixedPoint>`_
  ##  * `ceil proc <#ceil,FixedPoint>`_
  ##  * `floatPart proc <#floatPart,FixedPoint>`_
  ##  * `wholePart proc <#wholePart,FixedPoint>`_
  ##  * `modf proc <#modf,FixedPoint>`_
  ##  * `roundHalfUp proc <#roundHalfUp,FixedPoint>`_
  ##  * `roundHalfDown proc <#roundHalfDown,FixedPoint>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   var a = initQ16_16()
  ##   var w = initQ16_16()
  ##   var f = initQ16_16()
  ##   a.fromFloat(4.6)
  ##   (w,f) = modf(a)
  ##   w.toFloat() ## 4.0
  ##   f.toFloat() ## ~ 0.6
  ##   a.fromFloat(-4.3)
  ##   (w,f) = modf(a)
  ##   w.toFloat() ## -4.0
  ##   f.toFloat() ## ~ -0.3
  ##
  var f = floatPart(a)
  var i = wholePart(a)
  if isNeg(a):
    f.data = -f.data
  return (i, f)

proc fact*(a: FixedPoint) : FixedPoint =
  if a.isNeg():
    return low(a)
  if a.isZero() or a.isOne():
    return a.one()

  result = a.one()
  var i   = a.one()

  while (i <= a):
    result = ovMul(result, i)
    i   = ovAdd(i, a.one())

proc fact_noof*(a: FixedPoint) : (bool, FixedPoint) =
  result[0] = true
  if a.isNeg():
    return (false, low(a))
  if a.isZero() or a.isOne():
    return (false, a.one())

  result[1] = a.one()
  var i   = a.one()

  while (i <= a):
    if not isSafeMul(a, i):
      return (true, result[1])
    result[1] = ovMul(result[1], i)
    i   = ovAdd(i, a.one())

proc isqrt*(a: FixedPoint): FixedPoint =
  ## Return the integer square root of 'a'
  ##
  ## Example:
  ##
  ## .. code-block::: nim
  ##
  ##    a = fromInt(1234)
  ##    isqrt(a).toFloat() = 35.0 # normally: 35.128336
  ##

  if isNeg(a):
    result.data = low(typeof(a.data))
  else:
    result.data = isqrt(toInt(a)) shl a.fracBits


proc ipow*(a: FixedPoint, p: Natural): FixedPoint =
  ## Computes a to power raised of integer p.
  ## This method uses ovvMul
  ##
  ## Example:
  ##
  ## .. code-block::: nim
  ##
  ##    a = initQ16_16()
  ##    a.fromInt(2)
  ##    echo pow(a, 32).toInt() ## 65536
  ##

  if p == 0:
    return a.one()

  result.data = a.data
  var i = 1
  while i < p:
    result = ovMul(result, a)
    i += 1

proc ilog*(a: FixedPoint, base: FixedPoint): FixedPoint =
  ## Computes the largest integer whose power of positive integer base is less than or equal to positive a

  var b = a
  if b.isNeg() or base.isNeg():
    return low(b)

  while b >= base:
    result = ovAdd(result, b.one())
    b = ovDiv(b, base)

proc ilog10*(a: FixedPoint): FixedPoint =
  ## Convenient method to computes ilog with base 10
  var b = fromSameType(a)
  b.fromInt(10)
  ilog(a, b)

proc ilog2*(a: FixedPoint): FixedPoint =
  ## Convenient method to computes ilog with base 2
  var b = fromSameType(a)
  b.fromInt(2)
  ilog(a, b)
