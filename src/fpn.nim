import fpn/fpn
export fpn

# TODO: generate all numbers?
genQM_N(initQ16_16, 16, fixedPoint32)
genQM_N(initQ22_10, 10, fixedPoint32)
genQM_N(initQ6_10, 10, fixedPoint16)

export initQ16_16
export initQ22_10
export initQ6_10
